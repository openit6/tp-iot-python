# School Project IOT

## Setup

### To run the script outstide Docker

1. Create the `.env` file with the needed variables. See [Environnement Variables](#environnement-variables)
1. Create Python Venv
    `python -m venv <path to venv>`
1. Load the venv (on linux)
    `source <path to venv>/bin/activate`
1. Install the dependancies
    `pip install -r requirements.txt`
1. Start the script
    `python3 app.py`

### With Docker

1. Create the `.env` file with the needed variables. See [Environnement Variables](#environnement-variables)
1. Build the image
    `docker build . -t <image name>`
1. Run the image
    `docker run -it <image name>:latest`

## Environnement Variables

| Variable | Description                                      | Exemple             |
| -------- | ------------------------------------------------ | ------------------- |
| USERNAME | Username to connect to MQTT server (thingsboard) | supersecretusername |
| URL      | MQTT server host                                 | demo.thingsboard.io |
