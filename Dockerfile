FROM python:slim-bookworm
WORKDIR /app
COPY . /app
RUN pip install -r /app/requirements.txt

ENTRYPOINT [ "python3", "-u", "app.py" ]