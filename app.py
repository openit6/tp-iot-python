import os
import paho.mqtt.client as paho
from dotenv import load_dotenv
import random
import time

load_dotenv()

USERNAME = os.getenv("USERNAME")
URL = os.getenv("URL")

# Initialize The client
client = paho.Client(client_id="", userdata=None, protocol=paho.MQTTv311)
# Set the username & password
client.username_pw_set(USERNAME)
# Connect to the server
client.connect(URL, 1883)

for i in range(1, 100):
    # Generate random data
    temperature = random.randint(-30, 30)
    humidity = random.randint(0, 100)

    payload = {"temperature": temperature, "humidity": humidity}

    print(payload)
    # Publish to mqtt server
    client.publish("v1/devices/me/telemetry", payload=str(payload), qos=1)
    time.sleep(5)
